﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class матрца : MonoBehaviour
{
    public Image im;
    private GameObject Ver;
    private int i = 1;
    public Vector3 mousePos;
    public float rast = 0.5f;
    public int kolichestvo;
    private bool f1 = true;
    public LineRenderer line;
    public int schet = 0;

    void Start()
    {
        Ver = this.transform.GetChild(0).gameObject;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            GameObject go = Instantiate(Ver);
            go.transform.SetParent(this.transform, true);
            go.transform.localScale = Ver.transform.localScale;
            go.SetActive(true);
            go.transform.position = new Vector3(mousePos.x, mousePos.y, mousePos.z);
            go.transform.GetChild(0).gameObject.GetComponent<Text>().text = Convert.ToString(i);
            i++;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (int i = 0; i < this.transform.childCount; i++) 
            {
                Debug.Log(this.transform.GetChild(i).gameObject.name);
                GameObject gam = this.transform.GetChild(i).gameObject;
                Vector3 vec = gam.transform.position;
                if ((mousePos.x <vec.x + rast) && (mousePos.y < vec.y + rast) && ( mousePos.x > vec.x - rast) && (mousePos.y > vec.y - rast))
                {
                    if (f1 == true)
                    {
                        gam.transform.GetComponent<Image>().color = Color.yellow;
                        kolichestvo = i;
                        f1 = false;
                    }
                    else
                    {
                        if (this.transform.GetChild(kolichestvo).gameObject.name == gam.name) 
                        {
                            gam.transform.GetComponent<Image>().color = Color.blue;
                            this.transform.GetChild(kolichestvo).transform.GetComponent<Image>().color = Color.blue;
                            f1 = true;
                            Debug.Log("g");
                        }
                        else if (this.transform.GetChild(kolichestvo).gameObject.name != gam.name)
                        {
                            line = new GameObject("Line").AddComponent<LineRenderer>();
                            line.startColor = Color.red;
                            line.endColor = Color.red;
                            line.startWidth = 0.1f;
                            line.endWidth = 0.1f;
                            line.positionCount = 2;
                            line.useWorldSpace = true;
                            line.SetPosition(0, this.transform.GetChild(kolichestvo).gameObject.transform.position);
                            line.SetPosition(1, gam.transform.position);
                            gam.transform.GetComponent<Image>().color = Color.white;
                            this.transform.GetChild(kolichestvo).transform.GetComponent<Image>().color = Color.white;
                            f1 = true;
                        }
                    }
                }

            }
        }
    }
} 
